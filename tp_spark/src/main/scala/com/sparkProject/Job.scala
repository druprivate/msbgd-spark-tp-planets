package com.sparkProject

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature.{StringIndexer, VectorAssembler}
import org.apache.spark.ml.tuning.{ParamGridBuilder, TrainValidationSplit, TrainValidationSplitModel}
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

/**
  * A Job that loads raw "candidate" exo-planet data, pre-processes and cleans the data, saves the cleaned data and
  * trains a logistic regression model to classify planets as exo-planets or not. After the model has been trained and saved,
  * new data can be cleaned and fed to the model for prediction.
  */
object Job {

  /**
    * The main entry point for this job.
    *
    * @param args arguments, a valid combination of "-rawDataFile=file", "-cleanDataFile=file", "-modelFile=file",
    *             "-predict", "-verbose".
    */
  def main(args: Array[String]): Unit = {
    var rawDataFile:String = null // the path to a raw data set file
    var cleanDataFile:String = null // the path to a previously saved clean data set file
    var modelFile:String = null // the path to the previously saved trained model
    var predict:Boolean = false // whether to run the model to predict on a previously saved clean data set
    var verbose:Boolean = false // whether various traces and information have to be displayed
    var cleanDataDf: DataFrame = null // the clean data set loaded or processed

    // Parse parameters and options
    args.map((arg: String) => {
      val v: Array[String] = arg.split("=")
      (v(0), v(1))
    }).map({
      case ("-rawDataFile", v) => rawDataFile = v
      case ("-cleanDataFile", v) => cleanDataFile = v
      case ("-modelFile", v) => modelFile = v
      case ("-predict", v) => predict = v.toLowerCase() == "true"
      case ("-verbose", v) => verbose = v.toLowerCase() == "true"
      case (o, v) => println("Unrecognized option", o)
    })

    // Check validity of parameters and options passed
    if (rawDataFile != null && cleanDataFile == null) {
      System.err.println("Parameter -cleanDataFile must be set along with -rawDataFile parameter.")
      System.exit(1)
    }
    if (predict && modelFile == null) {
      System.err.println("Option -predict requires -modelFile parameter to be set.")
      System.exit(1)
    }

    // SparkSession configuration
    val spark = SparkSession
      .builder
      .master("local")
      .appName("Exo-planets TP_parisTech")
      .getOrCreate()

    import spark.implicits._

    val sc = spark.sparkContext

    sc.setLogLevel("ERROR")

    // Process raw data set
    if (rawDataFile != null) {
      // Load the raw data set
      val raw_data_df = spark.read // returns a DataFrameReader, giving access to methods 'options' and 'csv'
        .option("header", "true") // Use first line of all files as header
        .option("inferSchema", "true") // Automatically infer data types
        .option("comment", "#") // All lines starting with # are ignored
        .csv(rawDataFile)

      // Clean the raw data set
      cleanDataDf = cleanData(raw_data_df)

      // Save/store the cleaned data set
      cleanDataDf.coalesce(1)
        .write
        .mode("overwrite")
        .option("header", "true")
        .csv(cleanDataFile)
    }

    // Model train or prediction
    if (modelFile != null) {
      // Load clean data set if not already loaded
      if (cleanDataDf == null) {
        cleanDataDf = spark.read
          .option("header", "true")
          .option("inferSchema", "true")
          .option("comment", "#")
          .csv(cleanDataFile)
      }
      if (!predict) {
        // Train or retrain the model on the cleaned data set
        val model = trainModel(cleanDataDf)
        // Save the trained model
        model.write
          .overwrite()
          .save(modelFile)
      } else {
        // Predict using the cleaned data set
        try {
          // Attempt to load the saved model
          val model = TrainValidationSplitModel.load(modelFile)
          predictWithModel(model, cleanDataDf)
        } catch {
          case e: java.io.IOException => println("Model does not exists or is corrupted")
        }
      }
    }

    /**
      * Cleans the provided raw data set.
      *
      * @param rawDataset the raw data set.
      * @return a clean data set.
      */
    def cleanData(rawDataset: DataFrame): DataFrame = {

      if (verbose) {
        println("number of columns", rawDataset.columns.length)
        println("number of rows", rawDataset.count)
        rawDataset.printSchema()
        val columns = rawDataset.columns.slice(10, 20)
        rawDataset.select(columns.map(col): _*).show(50)
        rawDataset.select("koi_eccen_err1").distinct().show()
      }

      // Clean data set
      val cleanDataSet1 = rawDataset.filter($"koi_disposition" === "CONFIRMED" || $"koi_disposition" === "FALSE POSITIVE")
        .drop("koi_eccen_err1", "index", "kepid", "koi_fpflag_nt",
          "koi_fpflag_ss", "koi_fpflag_co", "koi_fpflag_ec",
          "koi_sparprov", "koi_trans_mod", "koi_datalink_dvr",
          "koi_datalink_dvs", "koi_tce_delivname", "koi_parm_prov",
          "koi_limbdark_mod", "koi_fittype", "koi_disp_prov",
          "koi_comment", "kepoi_name", "kepler_name", "koi_vet_date",
          "koi_pdisposition")

      val colsToDrop = cleanDataSet1.columns.filter(col => cleanDataSet1.groupBy(col).count().count() <= 1)
      val cleanDataSet2 = cleanDataSet1.drop(colsToDrop: _*)

      if (verbose)
        cleanDataSet2.printSchema()

      if (verbose)
        cleanDataSet2.stat.freqItems(cleanDataSet2.columns.slice(1, 5)).show()

      val cleanDataSet3 = cleanDataSet2.na.fill(0.0)

      val df_labels = cleanDataSet3.select("rowid", "koi_disposition")
      val df_features = cleanDataSet3.drop("koi_disposition")

      val cleanDataSet4 = df_labels.join(df_features, "rowid")

      def udf_sum = udf((col1: Double, col2: Double) => col1 + col2)

      val cleanDataSet5 = cleanDataSet4.withColumn("koi_ror_min", udf_sum($"koi_ror", $"koi_ror_err2"))
        .withColumn("koi_ror_max", $"koi_ror" + $"koi_ror_err1")

      return cleanDataSet5

    }

    /**
      * Trains on the provided data set a logistic regression model to predict the actual "Exo-planet" status of
      * candidate exo-planets.
      *
      * @param dataset the (clean) data set to train on
      * @return the trained logistic regression model
      */
    def trainModel(dataset: DataFrame): TrainValidationSplitModel = {

      if (verbose) {
        println("number of columns", dataset.columns.length)
        println("number of rows", dataset.count)
        dataset.printSchema()
      }

      val labelColumn = "koi_disposition"
      val inputColumns = cleanDataDf.columns.filter(col => !(col.equals(labelColumn) || col.equals("rowid")))

      // Split dataset randomly into Training and Test sets. Set seed for reproducibility
      // Split the dataset into training and test sets (10% held out for testing)
      val splits = cleanDataDf.randomSplit(Array(0.9, 0.1), seed = 1985L)
      val (trainingData, testData) = (splits(0), splits(1))
      trainingData.cache()
      testData.cache()

      // Create a feature vector from the input columns
      val assembler = new VectorAssembler()
        .setInputCols(inputColumns)
        .setOutputCol("features")

      // Convert target into numerical categories
      val labelIndexer = new StringIndexer()
        .setInputCol("koi_disposition")
        .setOutputCol("label")

      // Configure a logistic regression model
      val classifier = new LogisticRegression()
        .setElasticNetParam(1.0) // L1-norm regularization : LASSO
        .setLabelCol("label")
        .setStandardization(true) // scale the data
        .setFitIntercept(true) // we want an affine regression (with false, it is a linear regression)
        .setTol(1.0e-5) // stop criterion of the algorithm based on its convergence
        .setMaxIter(300) // a security stop criterion to avoid infinite loops
        .setFeaturesCol("features")

      // Chain tranformer and indexer in a Pipeline
      val pipeline = new Pipeline().setStages(Array(assembler, labelIndexer, classifier))

      // We use a ParamGridBuilder to construct a grid of parameters to search over.
      val x = (-6.0 to 0.0 by 0.5 toArray).map(math.pow(10, _))
      val paramGrid = new ParamGridBuilder()
        .addGrid(classifier.regParam, x)
        .build()

      // The estimator is a logistic regression pipeline.
      // A TrainValidationSplit requires an Estimator, a set of Estimator ParamMaps, and an Evaluator.
      val trainValidationSplit = new TrainValidationSplit()
        .setEstimator(pipeline)
        .setEvaluator(new MulticlassClassificationEvaluator)
        .setEstimatorParamMaps(paramGrid)
        // 70% of the data will be used for training and the remaining 30% for validation.
        .setTrainRatio(0.7)

      // Run train validation split, and choose the best set of parameters.
      val model = trainValidationSplit.fit(trainingData)

      // Make predictions on test data. model is the model with combination of parameters that performed best.
      val predictions = model.transform(testData)
        .select("features", "label", "prediction")

      predictions.groupBy("label", "prediction").count.show()

      if (verbose)
        predictions.show()

      val predictionAndLabel: RDD[(Double, Double)] = predictions.select("prediction", "label")
        .rdd.map {
        case Row(prediction: Double, label: Double) => (prediction, label)
      }

      if (verbose) {
        // Generate confusion matrix
        val metrics = new MulticlassMetrics(predictionAndLabel)

        println(metrics.confusionMatrix)

        println("Accuracy = " + metrics.accuracy)
      }

      return model

    }

    /**
      * Predicts actual "Exo-planet" status of each of the candidate exo-planets from the provided data set
      * using the provided model.
      *
      * @param model the logistic regression model
      * @param dataset the (clean) data set on which to run the prediction
      */
    def predictWithModel(model: TrainValidationSplitModel, dataset: DataFrame) = {

      if (verbose)
        cleanDataDf.printSchema()

      val labelColumn = "koi_disposition"
      val inputColumns = cleanDataDf.columns.filter(col => !(col.equals(labelColumn) || col.equals("rowid")))

      if (verbose)
        inputColumns.foreach(println)

      val testData = cleanDataDf
      testData.cache()

      // Make predictions on test data. model is the model with combination of parameters that performed best.
      val predictions = model.transform(testData)
        .select("features", "label", "prediction")

      predictions.groupBy("label", "prediction").count.show()

      if (verbose)
        predictions.show()

      val predictionAndLabel: RDD[(Double, Double)] = predictions.select("prediction", "label")
        .rdd.map {
        case Row(prediction: Double, label: Double) => (prediction, label)
      }

      if (verbose) {
        // Generate confusion matrix
        val metrics = new MulticlassMetrics(predictionAndLabel)

        println(metrics.confusionMatrix)

        println("Accuracy = " + metrics.accuracy)
      }

    }
  }
}
