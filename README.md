# tpSparkPlanets


## Job parameters
### Description
1. rawDataFile   : path to a raw data set file
2. cleanDataFile : path to a previously saved clean data set file
3. modelFile     : path to the previously saved trained model
4. predict       : whether to run the model to predict on a previously saved clean data set
5. verbose       : whether various traces and information have to be displayed
6. cleanDataDf   : clean data set loaded or processed

### Combinations
1. just cleaning : -rawDataFile={file} -cleanDataFile={file} -verbose={true|false}
2. train model : -modelFile={file} -cleanDataFile={file}
3. prediction : -cleanDataFile={file} -predict=true
4. all in one job (cleaning, training and prediction) : -rawDataFile={file} -modelFile={file} -cleanDataFile={file} -verbose={true|false}


## How to run the program
### Compilation
In terminal, run following command :
```{r, engine='bash', submit}
# change to local directory of build.sbt file
cd path_sbt_file/

# compilation command
sbt assembly
```

### Job submission
Example of job submission command :
```{r, engine='bash', submit}
# change to local directory
cd ~/spark-2.0.0-bin-hadoop2.6/bin

# define paths for data and source binary
TP_BIN_PATH="..." # path to source bin directory
TP_DATA_PATH="..." # path to data directory

# set master address
MASTER_ADDRESS="..."

# submit job combination 1 (just cleaning)
./spark-submit --conf spark.eventLog.enabled=true --conf spark.eventLog.dir="/tmp" --driver-memory 3G \
--executor-memory 4G --class com.sparkProject.Job --master spark://$MASTER_ADDRESS:7077 \
$TP_BIN_PATH/tp_spark-assembly-1.0.jar -rawDataFile=$TP_DATA_PATH/cumulative.csv \
-cleanDataFile=$TP_DATA_PATH/cleanDataFile -verbose=true

# submit job combination 2 (train model)
./spark-submit --conf spark.eventLog.enabled=true --conf spark.eventLog.dir="/tmp" --driver-memory 3G \
--executor-memory 4G --class com.sparkProject.Job --master spark://$MASTER_ADDRESS:7077 \
$TP_BIN_PATH/tp_spark-assembly-1.0.jar -modelFile=$TP_DATA_PATH/modelFile \
-cleanDataFile=$TP_DATA_PATH/cleanDataFile  -verbose=true

# submit job combination 3 (prediction)
./spark-submit --conf spark.eventLog.enabled=true --conf spark.eventLog.dir="/tmp" --driver-memory 3G \
--executor-memory 4G --class com.sparkProject.Job --master spark://$MASTER_ADDRESS:7077 \
$TP_BIN_PATH/tp_spark-assembly-1.0.jar -modelFile=$TP_DATA_PATH/modelFile \
-cleanDataFile=$TP_DATA_PATH/cleanDataFile  -predict -verbose=true

# submit job combination 4 (all in one job)
./spark-submit --conf spark.eventLog.enabled=true --conf spark.eventLog.dir="/tmp" --driver-memory 3G \
--executor-memory 4G --class com.sparkProject.Job --master spark://$MASTER_ADDRESS:7077 \
$TP_BIN_PATH/tp_spark-assembly-1.0.jar -rawDataFile=$TP_DATA_PATH/cumulative.csv \
-cleanDataFile=$TP_DATA_PATH/cleanDataFile -verbose=true -modelFile=$TP_DATA_PATH/modelFile -predict

```


## Results
13 values of regularization coefficient were tested, ranging from 10-6 to 1 on a logarithmic scale.


The figure below shows the accuracy of each corresponding classifier (logistic regression) in function of the logarithm of the regularization coefficient. The accuracy is very similar (values > 0.95) for regularization coefficients between 10-6 and 0.01, then suddenly decreases above 0.1.


![alt tag](https://raw.githubusercontent.com/drussier/tpSparkPlanets/master/scores.png)
